﻿using UnityEngine;

namespace RayWenderlich.Unity.StatePatternInUnity
{
    public class StandingState : GroundedState
    {
        private bool jump;
        private bool crouch;

        public StandingState(Character character, StateMachine stateMachine) : base(character, stateMachine)
        {
        }

        public override void Enter()
        {
            base.Enter();
            speed = character.MovementSpeed;
            rotationSpeed = character.RotationSpeed;
            jump = false;
            crouch = false;
        }

        public override void HandleInput()
        {
            base.HandleInput();
            crouch = Input.GetButtonDown("Fire3");
            jump = Input.GetKeyDown(KeyCode.Space);
        }

        public override void PhysicUpdate()
        {
            base.PhysicUpdate();
            if (crouch)
            {
                stateMachine.ChangeState(character.duckingState);
            }
            else if (jump)
            {
                stateMachine.ChangeState(character.jumpingState);
            }
        }
    }
}
